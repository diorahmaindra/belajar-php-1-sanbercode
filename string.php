<!DOCTYPE html>
<html>
<head>
	<title>String PHP</title>
</head>
<body>
	<h1>Berlatih String PHP</h1>
		<h3>Soal No. 1</h3>
		<!-- 
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
        -->

        <!-- Kalimat Pertama -->
        <?php 
	        $first_sentence = "Hello PHP!";
	        $panjangkata1 = strlen($first_sentence);
	        $jumlahkata1 = str_word_count($first_sentence);
	        echo "Kalimat Pertama : $first_sentence <br>";
	        echo "Panjang Kalimat : $panjangkata1 <br> ";
	        echo "Jumlah Kata : $jumlahkata1 <br>";
        ?>
        <br>
        <!-- Kalimat Kedua -->
        <?php 
        	$second_sentence = "I'm ready for the challenges";
	        $panjangkata2 = strlen($second_sentence);
	        $jumlahkata2 = str_word_count($second_sentence);
	        echo "Kalimat Kedua : $second_sentence <br>";
	        echo "Panjang Kalimat : $panjangkata2 <br> ";
	        echo "Jumlah Kata : $jumlahkata2 <br>";        	
        ?>

        <h3>Soal No.2</h3>
        <!--  
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya.  
        -->
        <?php
	        $string2 = "I love PHP";
	        echo "<label>String: </label> \"$string2\" <br>";
	        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ;
	        echo "Kata kedua: " . substr($string2, 2, 4) . "<br>" ;
	        echo "Kata ketiga: " . substr($string2, 7, 3) . "<br>" ;
        ?>  

        <h3>Soal No.3</h3>
        <!-- 
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        -->
        <?php  
	        $string3 = "PHP is old but sexy!";
	        echo "String: \"$string3\" <br>";
	        echo "Output : " .str_replace("sexy!", "awesome!", $string3); 
	        // OUTPUT : "PHP is old but awesome" 
        ?>        
</body>
</html>